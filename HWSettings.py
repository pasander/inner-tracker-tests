import copy

HWSettings = {
"nEvents"           :   100,
"nEvtsBurst"        :   100,

"VDDDTrimTarget"    :  1.30,
"VDDATrimTarget"    :  1.20,
"VDDDTrimTolerance" :  0.01,
"VDDATrimTolerance" :  0.01,

"nTRIGxEvent"       :    10,
"INJtype"           :     1,
"ResetMask"         :     0,
"ResetTDAC"         :     0,

"ROWstart"          :     0,
"ROWstop"           :   191,
"COLstart"          :   128,
"COLstop"           :   263,

"LatencyStart"      :     0,
"LatencyStop"       :   511,

"VCalHstart"        :   100,
"VCalHstop"         :   600,
"VCalHnsteps"       :    50,
"VCalMED"           :   100,

"TargetCharge"      : 20000,
"KrumCurrStart"     :     0,
"KrumCurrStop"      :   127,

"ThrStart"          :   380,
"ThrStop"           :   500,
"TargetThr"         :  2000,
"TargetOcc"         :  1e-6,
"UnstuckPixels"     :     0,

"TAP0Start"         :     0,
"TAP0Stop"          :  1023,
"TAP1Start"         :     0,
"TAP1Stop"          :   511,
"TAP2Start"         :     0,
"TAP2Stop"          :   511,
"InvTAP2"           :     0,

"chain2Test"        :     0,
"byTime"            :     1,
"framesORtime"      :    10,

"RegNameDAC1"       :   'VCAL_HIGH',
"StartValueDAC1"    :   250,
"StopValueDAC1"     :   600,
"StepDAC1"          :    50,
"RegNameDAC2"       :    'user.ctrl_regs.fast_cmd_reg_5.delay_after_inject_pulse',
"StartValueDAC2"    :    28,
"StopValueDAC2"     :    50,
"StepDAC2"          :     1,

"DisplayHisto"      :     0,
"UpdateChipCfg"     :     1,

"SaveBinaryData"    :     0,
"nHITxCol"          :     1,
"InjLatency"        :    32,
"nClkDelays"        :   280,

"DoSplitByHybrid"   :       0,
"StopIfCommFails"   :       1,
}

HWSettingsA = copy.deepcopy(HWSettings)
HWSettingsA["ResetTDAC"] = -1
HWSettingsA["TargetCharge"] = 10000
HWSettingsA["TDACGainStart"] = 130
HWSettingsA["TDACGainStop"] = 130
HWSettingsA["TDACGainNSteps"] = 0
HWSettingsA["DoNSteps"] = 0
HWSettingsA["OccPerPixel"] = 2e-5
HWSettingsA["MaxMaskedPixels"] = 1
HWSettingsA["DoOnlyNGroups"] = 0
HWSettingsA["DisableChannelsAtExit"] = 1
HWSettingsA["DataOutputDir"] = ""

#Adding CROC HW_settings
HWSettingsB = copy.deepcopy(HWSettingsA)
HWSettingsB["DoDataIntegrity"] = 0
HWSettingsB["ROWstop"] = 335
HWSettingsB["COLstart"] = 0
HWSettingsB["COLstop"] = 431
HWSettingsB["VCalHstop"] = 1100
HWSettingsB["KrumCurrStop"] = 210
HWSettingsB["TDACGainStart"] = 140
HWSettingsB["TDACGainStop"] = 140
HWSettingsB["VDDDTrimTarget"] = 1.20
HWSettingsB["RegNameDAC1"] = "user.ctrl_regs.fast_cmd_reg_5.delay_after_inject_pulse"
HWSettingsB["StartValueDAC1"] = 28
HWSettingsB["StopValueDAC1"] = 50
HWSettingsB["StepDAC1"] = 1
HWSettingsB["RegNameDAC2"] = "VCAL_HIGH"
HWSettingsB["StartValueDAC2"] = 300
HWSettingsB["StopValueDAC2"] = 1000
HWSettingsB["StepDAC2"] = 20
HWSettingsB["nClkDelays"] = 1300

#End section
HWSettingsA_Latency = copy.deepcopy(HWSettingsA)
HWSettingsA_Latency["LatencyStart"] = 110
HWSettingsA_Latency["LatencyStop"] = 150

HWSettingsA_PixelAlive = copy.deepcopy(HWSettingsA)

HWSettingsA_PixelAlive_90percent = copy.deepcopy(HWSettingsA_PixelAlive)
HWSettingsA_PixelAlive_90percent["OccPerPixel"] = 0.9

HWSettingsA_PixelAlive_Mask = copy.deepcopy(HWSettingsA_PixelAlive)
HWSettingsA_PixelAlive_Mask["ResetMask"] = 1
HWSettingsA_PixelAlive_Mask["ResetTDAC"] = 1

HWSettingsA_PixelAlive_Analog = copy.deepcopy(HWSettingsA_PixelAlive)
HWSettingsA_PixelAlive_Analog["INJtype"] = 1

HWSettingsA_PixelAlive_Digital = copy.deepcopy(HWSettingsA_PixelAlive)
HWSettingsA_PixelAlive_Digital["INJtype"] = 2

HWSettingsA_NoiseScan = copy.deepcopy(HWSettingsA)
HWSettingsA_NoiseScan["nEvents"] = 1e7
HWSettingsA_NoiseScan["nEvtsBurst"] = 1e4
HWSettingsA_NoiseScan["INJtype"] = 0
HWSettingsA_NoiseScan["nClkDelays"] = 10
HWSettingsA_NoiseScan["nTRIGxEvent"] = 1

HWSettingsA_SCurve =  copy.deepcopy(HWSettingsA)
HWSettingsA_SCurve_1100 =  copy.deepcopy(HWSettingsA_SCurve)
HWSettingsA_SCurve_1100["VCalHstop"] = 1100

HWSettingsA_SCurve_800 =  copy.deepcopy(HWSettingsA_SCurve)
HWSettingsA_SCurve_800["VCalHstop"] = 800

HWSettingsA_SCurve_600 =  copy.deepcopy(HWSettingsA_SCurve)
HWSettingsA_SCurve_600["VCalHstop"] = 600

HWSettingsA_SCurve_400 =  copy.deepcopy(HWSettingsA_SCurve)
HWSettingsA_SCurve_400["VCalHstop"] = 400

HWSettingsA_ThresEqu = copy.deepcopy(HWSettingsA)
HWSettingsA_ThresEqu["nEvents"] = 100
HWSettingsA_ThresEqu["nEvtsBurst"] = 100

HWSettingsA_ThresEqu_1Step = copy.deepcopy(HWSettingsA_ThresEqu)
HWSettingsA_ThresEqu_1Step["DoNSteps"] = 1

HWSettingsA_GainScan = copy.deepcopy(HWSettingsA)
HWSettingsA_GainScan["VCalHstop"] = 4000
HWSettingsA_GainScan["VCalHnsteps"] = 20

HWSettingsA_GainOpt = copy.deepcopy(HWSettingsA)
HWSettingsA_GainOpt["VCalHstop"] = 4000
HWSettingsA_GainOpt["VCalHnsteps"] = 20

HWSettingsA_ThresMin = copy.deepcopy(HWSettingsA)
HWSettingsA_ThresMin["nEvents"] = 1e7
HWSettingsA_ThresMin["nEvtsBurst"] = 1e4
HWSettingsA_ThresMin["INJtype"] = 0
HWSettingsA_ThresMin["nClkDelays"] = 10
HWSettingsA_ThresMin["ThrStart"] = 370
HWSettingsA_ThresMin["nTRIGxEvent"] = 1

HWSettingsA_ThresAdj = copy.deepcopy(HWSettingsA)

HWSettingsA_ThresAdj_3000 = copy.deepcopy(HWSettingsA_ThresAdj)
HWSettingsA_ThresAdj_3000["TargetThr"] = 3000

HWSettingsA_ThresAdj_2000 = copy.deepcopy(HWSettingsA_ThresAdj)
HWSettingsA_ThresAdj_2000["TargetThr"] = 2000

HWSettingsA_ThresAdj_1500 = copy.deepcopy(HWSettingsA_ThresAdj)
HWSettingsA_ThresAdj_1500["TargetThr"] = 1500

HWSettingsA_ThresAdj_1200 = copy.deepcopy(HWSettingsA_ThresAdj)
HWSettingsA_ThresAdj_1200["TargetThr"] = 1200

HWSettingsA_ThresAdj_1000 = copy.deepcopy(HWSettingsA_ThresAdj)
HWSettingsA_ThresAdj_1000["TargetThr"] = 1000

HWSettingsA_InjDelay = copy.deepcopy(HWSettingsA)
HWSettingsA_InjDelay["LatencyStart"] = 110
HWSettingsA_InjDelay["nTRIGxEvent"] = 1
HWSettingsA_InjDelay["DoOnlyNGroups"] = 1

HWSettingsA_ClockDelay = copy.deepcopy(HWSettingsA)
HWSettingsA_ClockDelay["nTRIGxEvent"] = 1
HWSettingsA_ClockDelay["DoOnlyNGroups"] = 1

HWSettingsA_BitErrRate = copy.deepcopy(HWSettingsA)

HWSettingsA_DataRBOpt = copy.deepcopy(HWSettingsA)

HWSettingsA_VoltageTuning = copy.deepcopy(HWSettingsA)

HWSettingsA_GenDACDAC = copy.deepcopy(HWSettingsA)

HWSettingsA_Physics = copy.deepcopy(HWSettingsA)

#For CROC
HWSettingsB_Latency = copy.deepcopy(HWSettingsB)
HWSettingsB_Latency["LatencyStart"] = 110
HWSettingsB_Latency["LatencyStop"] = 150

HWSettingsB_PixelAlive = copy.deepcopy(HWSettingsB)
HWSettingsB_PixelAlive["DoDataIntegrity"] = 2
HWSettingsB_PixelAlive["OccPerPixel"] = 0.9

HWSettingsB_PixelAlive_90percent = copy.deepcopy(HWSettingsB_PixelAlive)
HWSettingsB_PixelAlive_90percent["OccPerPixel"] = 0.9
HWSettingsB_PixelAlive["DoDataIntegrity"] = 2

HWSettingsB_PixelAlive_Mask = copy.deepcopy(HWSettingsB_PixelAlive)
HWSettingsB_PixelAlive_Mask["ResetMask"] = 1
HWSettingsB_PixelAlive_Mask["ResetTDAC"] = 1
HWSettingsB_PixelAlive["DoDataIntegrity"] = 2

HWSettingsB_PixelAlive_Analog = copy.deepcopy(HWSettingsB_PixelAlive)
HWSettingsB_PixelAlive_Analog["INJtype"] = 1
HWSettingsB_PixelAlive["DoDataIntegrity"] = 2

HWSettingsB_PixelAlive_Digital = copy.deepcopy(HWSettingsB_PixelAlive)
HWSettingsB_PixelAlive_Digital["INJtype"] = 2
HWSettingsB_PixelAlive["DoDataIntegrity"] = 2

HWSettingsB_NoiseScan = copy.deepcopy(HWSettingsB)
HWSettingsB_NoiseScan["nEvents"] = 1e6
HWSettingsB_NoiseScan["nEvtsBurst"] = 1e4
HWSettingsB_NoiseScan["INJtype"] = 0
HWSettingsB_NoiseScan["nClkDelays"] = 70
HWSettingsB_NoiseScan["nTRIGxEvent"] = 10

HWSettingsB_SCurve =  copy.deepcopy(HWSettingsB)

HWSettingsB_SCurve_1100 =  copy.deepcopy(HWSettingsB_SCurve)
HWSettingsB_SCurve_1100["VCalHstop"] = 1100

HWSettingsB_SCurve_800 =  copy.deepcopy(HWSettingsB_SCurve)
HWSettingsB_SCurve_800["VCalHstop"] = 800

HWSettingsB_SCurve_600 =  copy.deepcopy(HWSettingsB_SCurve)
HWSettingsB_SCurve_600["VCalHstop"] = 600

HWSettingsB_SCurve_400 =  copy.deepcopy(HWSettingsB_SCurve)
HWSettingsB_SCurve_400["VCalHstop"] = 400

HWSettingsB_ThresEqu = copy.deepcopy(HWSettingsB)
HWSettingsB_ThresEqu["nEvents"] = 100
HWSettingsB_ThresEqu["nEvtsBurst"] = 100
#HWSettingsB_ThresEqu["TDACGainStart"] = 100
#HWSettingsB_ThresEqu["TDACGainStop"] = 160
#HWSettingsB_ThresEqu["TDACGainNSteps"] = 12

HWSettingsB_ThresEqu_1Step = copy.deepcopy(HWSettingsB_ThresEqu)
HWSettingsB_ThresEqu_1Step["DoNSteps"] = 1

HWSettingsB_GainScan = copy.deepcopy(HWSettingsB)
HWSettingsB_GainScan["VCalHstop"] = 4000
HWSettingsB_GainScan["VCalHnsteps"] =50

HWSettingsB_GainOpt = copy.deepcopy(HWSettingsB)
HWSettingsB_GainOpt["VCalHstop"] = 4000
HWSettingsB_GainOpt["VCalHnsteps"] = 20

HWSettingsB_ThresMin = copy.deepcopy(HWSettingsB)
HWSettingsB_ThresMin["nEvents"] = 1e7
HWSettingsB_ThresMin["nEvtsBurst"] = 1e4
HWSettingsB_ThresMin["INJtype"] = 0
HWSettingsB_ThresMin["nClkDelays"] = 70
HWSettingsB_ThresMin["ThrStart"] = 370
HWSettingsB_ThresMin["nTRIGxEvent"] = 1

HWSettingsB_ThresAdj = copy.deepcopy(HWSettingsB)
HWSettingsB_ThresAdj["ThrStart"] = 380
HWSettingsB_ThresAdj["ThrStop"] = 500

HWSettingsB_ThresAdj_3000 = copy.deepcopy(HWSettingsB_ThresAdj)
HWSettingsB_ThresAdj_3000["TargetThr"] = 3000

HWSettingsB_ThresAdj_2000 = copy.deepcopy(HWSettingsB_ThresAdj)
HWSettingsB_ThresAdj_2000["TargetThr"] = 2000

HWSettingsB_ThresAdj_1500 = copy.deepcopy(HWSettingsB_ThresAdj)
HWSettingsB_ThresAdj_1500["TargetThr"] = 1500

HWSettingsB_ThresAdj_1200 = copy.deepcopy(HWSettingsB_ThresAdj)
HWSettingsB_ThresAdj_1200["TargetThr"] = 1200

HWSettingsB_ThresAdj_1000 = copy.deepcopy(HWSettingsB_ThresAdj)
HWSettingsB_ThresAdj_1000["TargetThr"] = 1000

HWSettingsB_InjDelay = copy.deepcopy(HWSettingsB)
HWSettingsB_InjDelay["LatencyStart"] = 110
HWSettingsB_InjDelay["LatencyStop"] = 150
HWSettingsB_InjDelay["nTRIGxEvent"] = 1
HWSettingsB_InjDelay["DoOnlyNGroups"] = 1

HWSettingsB_ClockDelay = copy.deepcopy(HWSettingsB)
HWSettingsB_ClockDelay["nTRIGxEvent"] = 1
HWSettingsB_ClockDelay["DoOnlyNGroups"] = 1

HWSettingsB_BitErrRate = copy.deepcopy(HWSettingsB)

HWSettingsB_DataRBOpt = copy.deepcopy(HWSettingsB)

HWSettingsB_VoltageTuning = copy.deepcopy(HWSettingsB)

HWSettingsB_GenDACDAC = copy.deepcopy(HWSettingsB)

HWSettingsB_Physics = copy.deepcopy(HWSettingsB)

#End


HWSettings_DictA = {
    'Latency'                    :    HWSettingsA_Latency,
    'PixelAlive'                 :    HWSettingsA_PixelAlive,
    'PixelAlive_Mask'            :    HWSettingsA_PixelAlive_Mask,
    'PixelAlive_90percent'       :    HWSettingsA_PixelAlive_90percent,
    'PixelAlive_Digital'        :       HWSettingsA_PixelAlive_Digital,
    'PixelAlive_Analog'         :       HWSettingsA_PixelAlive_Analog,
    'NoiseScan'                  :    HWSettingsA_NoiseScan,
    'GainScan'                   :    HWSettingsA_GainScan,
    'SCurveScan'                 :    HWSettingsA_SCurve,
    'SCurveScan_1100'            :    HWSettingsA_SCurve_1100,
    'SCurveScan_800'             :    HWSettingsA_SCurve_800,
    'SCurveScan_600'             :    HWSettingsA_SCurve_600,
    'SCurveScan_400'             :    HWSettingsA_SCurve_400,
    'ThresholdEqualization'      :    HWSettingsA_ThresEqu,
    'ThresholdEqualization_1Step':    HWSettingsA_ThresEqu_1Step,
    'GainOptimization'           :    HWSettingsA_GainOpt,
    'ThresholdMinimization'      :    HWSettingsA_ThresMin,
    'ThresholdAdjustment_3000'   :    HWSettingsA_ThresAdj_3000,
    'ThresholdAdjustment_2000'   :    HWSettingsA_ThresAdj_2000,
    'ThresholdAdjustment_1500'   :    HWSettingsA_ThresAdj_1500,
    'ThresholdAdjustment_1200'   :    HWSettingsA_ThresAdj_1200,
    'ThresholdAdjustment_1000'   :    HWSettingsA_ThresAdj_1000,
    'InjectionDelay'             :    HWSettingsA_InjDelay,
    'ClockDelay'                 :    HWSettingsA_ClockDelay,
    'BitErrorRate'               :    HWSettingsA_BitErrRate,
    'DataRBOptimization'         :    HWSettingsA_DataRBOpt,
    'ChipIntVoltageTuning'       :    HWSettingsA_VoltageTuning,
    'GenericDAC-DAC'             :    HWSettingsA_GenDACDAC,
    'Physics'                    :    HWSettingsA_Physics,
    'IVCurve'                    :    HWSettingsA,
    'SLDOScan'                   :    HWSettingsA,
}

HWSettings_DictB = {
    'Latency'                    :    HWSettingsB_Latency,
    'PixelAlive'                 :    HWSettingsB_PixelAlive,
    'PixelAlive_Mask'            :    HWSettingsB_PixelAlive_Mask,
    'PixelAlive_90percent'       :    HWSettingsB_PixelAlive_90percent,
    'PixelAlive_Digital'        :       HWSettingsB_PixelAlive_Digital,
    'PixelAlive_Analog'         :       HWSettingsB_PixelAlive_Analog,
    'NoiseScan'                  :    HWSettingsB_NoiseScan,
    'GainScan'                   :    HWSettingsB_GainScan,
    'SCurveScan'                 :    HWSettingsB_SCurve,
    'SCurveScan_1100'            :    HWSettingsB_SCurve_1100,
    'SCurveScan_800'             :    HWSettingsB_SCurve_800,
    'SCurveScan_600'             :    HWSettingsB_SCurve_600,
    'SCurveScan_400'             :    HWSettingsB_SCurve_400,
    'ThresholdEqualization'      :    HWSettingsB_ThresEqu,
    'ThresholdEqualization_1Step':    HWSettingsB_ThresEqu_1Step,
    'GainOptimization'           :    HWSettingsB_GainOpt,
    'ThresholdMinimization'      :    HWSettingsB_ThresMin,
    'ThresholdAdjustment_3000'   :    HWSettingsB_ThresAdj_3000,
    'ThresholdAdjustment_2000'   :    HWSettingsB_ThresAdj_2000,
    'ThresholdAdjustment_1500'   :    HWSettingsB_ThresAdj_1500,
    'ThresholdAdjustment_1200'   :    HWSettingsB_ThresAdj_1200,
    'ThresholdAdjustment_1000'   :    HWSettingsB_ThresAdj_1000,
    'InjectionDelay'             :    HWSettingsB_InjDelay,
    'ClockDelay'                 :    HWSettingsB_ClockDelay,
    'BitErrorRate'               :    HWSettingsB_BitErrRate,
    'DataRBOptimization'         :    HWSettingsB_DataRBOpt,
    'ChipIntVoltageTuning'       :    HWSettingsB_VoltageTuning,
    'GenericDAC-DAC'             :    HWSettingsB_GenDACDAC,
    'Physics'                    :    HWSettingsB_Physics,
    'IVCurve'                    :    HWSettingsB,
    'SLDOScan'                   :    HWSettingsB,
}
