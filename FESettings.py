import sys
import os
import copy
#Ugly fix that should work with any setup
hw_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(hw_dir)
import HWSettings

FESettings = {
'PA_IN_BIAS_LIN'        :  "350",
'FC_BIAS_LIN'           :   "20",
'KRUM_CURR_LIN'         :   "29",
'LDAC_LIN'              :  "130",
'COMP_LIN'              :  "110",
'REF_KRUM_LIN'          :  "300",
'Vthreshold_LIN'        :  "400",

'IBIASP1_SYNC'          :  "100",
'IBIASP2_SYNC'          :  "150",
'IBIAS_SF_SYNC'         :   "80",
'IBIAS_KRUM_SYNC'       :   "55",
'IBIAS_DISC_SYNC'       :  "280",
'ICTRL_SYNCT_SYNC'      :  "100",
'VBL_SYNC'              :  "360",
'VTH_SYNC'              :  "150",
'VREF_KRUM_SYNC'        :  "450",
'CONF_FE_SYNC'          :    "2",

'PRMP_DIFF'             :  "511",
'FOL_DIFF'              :  "542",
'PRECOMP_DIFF'          :  "512",
'COMP_DIFF'             : "1023",
'VFF_DIFF'              :   "40",
'VTH1_DIFF'             :  "700",
'VTH2_DIFF'             :  "100",
'LCC_DIFF'              :   "20",
'CONF_FE_DIFF'          :    "0",

'VCAL_HIGH'             :  "600",
'VCAL_MED'              :  "100",

'GP_LVDS_ROUTE'         :    "0",
'LATENCY_CONFIG'        :  "136",
'CLK_DATA_DELAY'        :    "0",
'CAL_EDGE_FINE_DELAY'   :    "0",
'ANALOG_INJ_MODE'       :    "0",

'VOLTAGE_TRIM_DIG'      :   "16",
'VOLTAGE_TRIM_ANA'      :   "16",

'CML_CONFIG_SER_EN_TAP' : "0b00",
'CML_CONFIG_SER_INV_TAP': "0b00",
'DAC_CML_BIAS_0'        :  "500",
'DAC_CML_BIAS_1'        :    "0",
'DAC_CML_BIAS_2'        :    "0",

'MONITOR_CONFIG_ADC'    :    "5",
'MONITOR_CONFIG_BG'     :   "12",

'RESISTORI2V'           :"10000",
'ADC_OFFSET_VOLT'       :   "63",
'ADC_MAXIMUM_VOLT'      :  "839",
'TEMPSENS_IDEAL_FACTOR' : "1225",
'SAMPLE_N_TIMES'        :   "10",
'VREF_ADC'              :  "900",
}

FESettingsB = {
    'DAC_PREAMP_L_LIN'       :   "300",
    'DAC_PREAMP_R_LIN'       :   "300",
    'DAC_PREAMP_TL_LIN'      :   "300",
    'DAC_PREAMP_TR_LIN'      :   "300",
    'DAC_PREAMP_T_LIN'       :   "300",
    'DAC_PREAMP_M_LIN'       :   "300",
    'DAC_FC_LIN'             :    "20",
    'DAC_KRUM_CURR_LIN'      :    "70",
    'DAC_REF_KRUM_LIN'       :   "360",
    'DAC_COMP_LIN'           :   "110",
    'DAC_COMP_TA_LIN'        :   "110",
    'DAC_GDAC_L_LIN'       :     "450",
    'DAC_GDAC_R_LIN'        :    "450",
    'DAC_GDAC_M_LIN'         :   "450",
    'DAC_LDAC_LIN'           :   "140",

    'VCAL_HIGH'              :  "2000",
    'VCAL_MED'               :   "100",

    'GP_LVDS_ROUTE_0'        :  "1495",
    'GP_LVDS_ROUTE_1'        :  "1495",
    'TriggerConfig'          :   "136",
    'CLK_DATA_DELAY'         :     "0",
    'CAL_EDGE_FINE_DELAY'    :     "0",
    'ANALOG_INJ_MODE'        :     "0",
    'SEL_CAL_RANGE'          :     "0",

    'SelfTriggerEn'         :       "0",
    'SelfTriggerDelay'      :       "30",
    "EnOutputDataChipId"    :       "1",

    'VOLTAGE_TRIM_DIG'       :     "8",
    'VOLTAGE_TRIM_ANA'       :     "8", 

    'CML_CONFIG_SER_EN_TAP'  :  "0b00",
    'CML_CONFIG_SER_INV_TAP' :  "0b00",
    'DAC_CML_BIAS_0'         :   "500",
    'DAC_CML_BIAS_1'         :     "0",
    'DAC_CML_BIAS_2'         :     "0",

    'MON_ADC_TRIM'           :     "5",

    'ToT6to4Mapping'         :     "0",
    'ToTDualEdgeCount'       :     "0",

    'RESISTORI2V'           :       "5000",
    'ADC_OFFSET_VOLT'        :    "63",
    'ADC_MAXIMUM_VOLT'       :   "839",
    'TEMPSENS_IDEAL_FACTOR'  :  "1225",
    'SAMPLE_N_TIMES'         :  "10",
    'VREF_ADC'               :   "800",
    'WAIT_MUX_CONFIG'       :   "100",
    
    "EN_CORE_COL_0"       :      "65535",
    "EN_CORE_COL_1"       :     "65535",
    "EN_CORE_COL_2"       :     "65535",
    "EN_CORE_COL_3"       :         "63",
}

# Only update this register. Otherwise, the calibration is destroyed
FESettings_DictA = {}
for key in HWSettings.HWSettings_DictA:
    FESettings_DictA[key] = FESettings
'''    
FESettings_DictA = {
    'Latency'                   :    FESettings,
    'PixelAlive'                :    FESettings,
    'NoiseScan'                 :    FESettings,
    'GainScan'                  :    FESettings,
    'SCurveScan'                :    FESettings,
    'ThresholdEqualization'     :    FESettings,
    'GainOptimization'          :    FESettings,
    'ThresholdMinimization'     :    FESettings,
    'ThresholdAdjustment'       :    FESettings,
    'InjectionDelay'            :    FESettings,
    'ClockDelay'                :    FESettings,
    'Physics'                   :    FESettings,
    'IVCurve'                   :    FESettings,
    'SLDOScan'                  :    FESettings,
}
'''
#The following automatically syncs the test names between FESettings and HWSettings. If we need to change specific ones, we can do so with 
#the addition of lines making explicit changes to the values in the map, such as FESettings_DictB['PixelAlive']=FESettingsB_PixelAlive
#or something like that.

FESettings_DictB = {}
for key in HWSettings.HWSettings_DictB:
    FESettings_DictB[key] = FESettingsB
'''  
FESettings_DictB = {
    'Latency'                    :    FESettingsB,
    'PixelAlive'                 :    FESettingsB,
    'PixelAlive_Mask'            :    FESettingsB,
    'PixelAlive_90percent'       :    FESettingsB,
    'NoiseScan'                  :    FESettingsB,
    'GainScan'                   :    FESettingsB,
    'SCurveScan'                 :    FESettingsB,
    'SCurveScan_600'             :    FESettingsB,
    'SCurveScan_400'             :    FESettingsB,
    'ThresholdEqualization'      :    FESettingsB,
    'ThresholdEqualization_1Step':    FESettingsB,
    'GainOptimization'           :    FESettingsB,
    'ThresholdMinimization'      :    FESettingsB,
    'ThresholdAdjustment_3000'   :    FESettingsB,
    'ThresholdAdjustment_2000'   :    FESettingsB,
    'ThresholdAdjustment_1500'   :    FESettingsB,
    'ThresholdAdjustment_1200'   :    FESettingsB,
    'ThresholdAdjustment_1000'   :    FESettingsB,
    'InjectionDelay'             :    FESettingsB,
    'ClockDelay'                 :    FESettingsB,
    'BitErrorRate'               :    FESettingsB,
    'DataRBOptimization'         :    FESettingsB,
    'ChipIntVoltageTuning'       :    FESettingsB,
    'GenericDAC-DAC'             :    FESettingsB,
    'Physics'                    :    FESettingsB,
    'IVCurve'                    :    FESettingsB,
    'SLDOScan'                   :    FESettingsB,
}
'''
